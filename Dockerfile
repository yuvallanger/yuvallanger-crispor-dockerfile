FROM ubuntu
MAINTAINER yuval.langer@gmail.com

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get install -y bwa
RUN apt-get install -y python-pip
RUN apt-get install -y python-matplotlib
RUN apt-get install -y apache2

RUN python2.7 -m pip install biopython numpy==1.14.0 scikit-learn==0.16.1 pandas twobitreader

RUN python2.7 -m pip install keras tensorflow h5py

RUN apt-get install -y r-base-core

RUN Rscript -e 'install.packages(c("e1071"),  repos="http://cran.rstudio.com/")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite(c("limma"));'

RUN apt-get install -y git

WORKDIR /var/www/html/
RUN git clone https://github.com/maximilianh/crisporWebsite

WORKDIR /var/www/html/crisporWebsite/
RUN mv genomes.sample genomes
RUN chmod +xr crispor.py

RUN echo 1
RUN a2enmod cgi
RUN rm /etc/apache2/sites-enabled/*
RUN echo '<VirtualHost *:80>' >> /etc/apache2/sites-available/crispor.conf
RUN echo '    ServerAdmin webmaster@localhost' >> /etc/apache2/sites-available/crispor.conf
RUN echo '    DocumentRoot /var/www/html' >> /etc/apache2/sites-available/crispor.conf
RUN echo '    ErrorLog ${APACHE_LOG_DIR}/error.log' >> /etc/apache2/sites-available/crispor.conf
RUN echo '    CustomLog ${APACHE_LOG_DIR}/access.log combined' >> /etc/apache2/sites-available/crispor.conf
RUN echo '    <Directory "/var/www/html">' >> /etc/apache2/sites-available/crispor.conf
RUN echo '        AllowOverride All' >> /etc/apache2/sites-available/crispor.conf
RUN echo '        Options +ExecCGI' >> /etc/apache2/sites-available/crispor.conf
RUN echo '        AddHandler cgi-script .cgi .pl .py' >> /etc/apache2/sites-available/crispor.conf
RUN echo '    </Directory>' >> /etc/apache2/sites-available/crispor.conf
RUN echo '</VirtualHost>' >> /etc/apache2/sites-available/crispor.conf
RUN ln -s /etc/apache2/sites-available/crispor.conf /etc/apache2/sites-enabled/crispor.conf
RUN echo
RUN service apache2 restart
RUN service apache2 status

CMD ["echo", "Image created"]
